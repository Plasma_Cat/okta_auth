﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.AspNetCore;
using GlobalLoop.AspNetCore;
using GlobalLoop.AspNetCore.Filters;
using GlobalLoop.Authorization.Data;
using GlobalLoop.Authorization.WebApi.Swagger;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace GlobalLoop.Authorization.WebApi
{
    public partial class Startup
    {
        private const string CorsPolicyName = "DefaultCorsPolicy";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public ILoggerFactory LoggerFactory { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvcCore(options =>
                {
                    options.Filters.Add<GlobalLoopExceptionFilterAttribute>();
                    options.Filters.Add<LogHttpContextBodyAttribute>();
                    options.Filters.Add<LogHttpContextUserAttribute>();
                    options.Filters.Add<RequestIdHeaderResponseAttribute>();
                    options.Filters.Add<ValidateModelAttribute>();

                    options.EnableEndpointRouting = false;
                })
                .AddApiExplorer()
                .AddAuthorization()
                .AddCors(options =>
                {
                    options.AddPolicy(CorsPolicyName,
                        builder =>
                        {
                            builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .WithExposedHeaders(AspNetCoreDefaults.HttpHeaders.RequestId);
                        });
                })
                .AddFormatterMappings()
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                })
                .AddNewtonsoftJson()
                .AddJsonOptions(o => o.JsonSerializerOptions.WriteIndented = false)
                .AddFluentValidation(cfg =>
                {
                    cfg.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                    cfg.RegisterValidatorsFromAssembly(typeof(Validation.Properties.Resources).Assembly);
                });

            AddSwagger(services);

            AddAuth(services);

            services.AddRequestContextAccessor();

            services.AddAutoMapper(typeof(Data.Properties.Resources), typeof(Handlers.Properties.Resources));

            services.AddMediatR(typeof(Handlers.Properties.Resources));

            AddData(services);

            services.Configure<OktaOptions>(Configuration.GetSection("OktaOptions"));
            services.AddTransient<IPasswordHasher<Data.Entities.User>>(_ => new PasswordHasher<Data.Entities.User>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory, IWebHostEnvironment env)
        {
            LoggerFactory = loggerFactory;

            app.UseCors(CorsPolicyName);

            app.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = context =>
                {
                    if (context.Response.StatusCode == 500)
                    {
                        context.Response.Headers.Add(CorsConstants.AccessControlAllowCredentials, "true");
                        context.Response.Headers.Add(CorsConstants.AccessControlExposeHeaders, AspNetCoreDefaults.HttpHeaders.RequestId);
                    }

                    context.Response.Headers.Add(AspNetCoreDefaults.HttpHeaders.RequestId, context.TraceIdentifier);
                    return Task.CompletedTask;
                }
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseSwagger();
            app.UseSwaggerUI(x => Configuration.Bind("SwaggerUIOptions", x));

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            using var scope = app.ApplicationServices.CreateScope();
            scope.ServiceProvider.GetRequiredService<AuthorizationDbContext>().Database.Migrate();
        }

        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.CustomSchemaIds(x => x.FullName.Substring(x.FullName.LastIndexOf('.')).Replace("+", string.Empty, StringComparison.OrdinalIgnoreCase));
                Configuration.Bind("SwaggerGenOptions", options);
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    [new OpenApiSecurityScheme { Reference = new OpenApiReference { Id = "Bearer", Type = ReferenceType.SecurityScheme } }] = Array.Empty<string>()
                });
                options.DocumentFilter<TokenOperation>(Configuration["OpenIdConnectServerOptions:TokenEndpointPath"]);
            });
        }

        private void AddData(IServiceCollection services)
        {
            services.AddDbContext<AuthorizationDbContext>(options => options.UseSqlServer(
               Configuration.GetConnectionString("AmkAuth"),
               x => x.MigrationsAssembly("GlobalLoop.Authorization.Data")));
        }
    }
}