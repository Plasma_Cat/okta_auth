﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Collections.Generic;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GlobalLoop.Authorization.WebApi.Swagger
{
    public class TokenOperation : IDocumentFilter
    {
        private readonly string _tokenEndpointPath;

        public TokenOperation(string tokenEndpointPath)
        {
            _tokenEndpointPath = tokenEndpointPath;
        }

        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Paths.Add(_tokenEndpointPath + "?refresh_token", new OpenApiPathItem
            {
                Operations = new Dictionary<OperationType, OpenApiOperation>()
                {
                    [OperationType.Post] = new OpenApiOperation
                    {
                        Tags = new[] { new OpenApiTag { Name = "Token" } },
                        OperationId = "Token_RefreshToken",
                        Responses = new OpenApiResponses
                        {
                        },
                        RequestBody = new OpenApiRequestBody
                        {
                            Required = true,
                            Content = new Dictionary<string, OpenApiMediaType>
                            {
                                ["application/x-www-form-urlencoded"] = new OpenApiMediaType
                                {
                                    Schema = new OpenApiSchema
                                    {
                                        Properties = new Dictionary<string, OpenApiSchema>
                                        {
                                            ["grant_type"] = new OpenApiSchema { Type = "string", Default = new OpenApiString("refresh_token") },
                                            ["refresh_token"] = new OpenApiSchema { Type = "string" },
                                        },
                                        Required = new HashSet<string> { "grant_type", "refresh_token" }
                                    }
                                }
                            }
                        }
                    }
                }
            });
            swaggerDoc.Paths.Add(_tokenEndpointPath + "?password", new OpenApiPathItem
            {
                Operations = new Dictionary<OperationType, OpenApiOperation>()
                {
                    [OperationType.Post] = new OpenApiOperation
                    {
                        Tags = new[] { new OpenApiTag { Name = "Token" } },
                        OperationId = "Token_Password",
                        Responses = new OpenApiResponses
                        {
                        },
                        RequestBody = new OpenApiRequestBody
                        {
                            Required = true,
                            Content = new Dictionary<string, OpenApiMediaType>
                            {
                                ["application/x-www-form-urlencoded"] = new OpenApiMediaType
                                {
                                    Schema = new OpenApiSchema
                                    {
                                        Properties = new Dictionary<string, OpenApiSchema>
                                        {
                                            ["grant_type"] = new OpenApiSchema { Type = "string" },
                                            ["username"] = new OpenApiSchema { Type = "string" },
                                            ["password"] = new OpenApiSchema { Type = "string" },
                                        },
                                        Required = new HashSet<string> { "grant_type", "username", "password" }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    }
}