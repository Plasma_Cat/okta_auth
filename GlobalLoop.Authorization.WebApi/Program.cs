﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace GlobalLoop.Authorization.WebApi
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await CreateHostBuilder(args).Build().RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, builder) =>
                {
                    builder.SetBasePath(Directory.GetCurrentDirectory());
                    builder.AddEnvironmentVariables();

                    builder
                        .AddJsonFile("appsettings.json", true, true)
                        .AddJsonFile("appsettings." + context.HostingEnvironment.EnvironmentName + ".json", true, true);

                    builder.AddJsonFile("Settings/swaggersettings.json", true, true);
                    builder.AddJsonFile("Settings/serilogsettings.json", true, true);
                    builder.AddJsonFile("Settings/authsettings.json", true, true);
                    builder.AddJsonFile("Settings/dbsettings.json", true, true);
                })
                .UseSerilog((context, configuration) =>
                {
                    configuration.ReadFrom.Configuration(context.Configuration);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}