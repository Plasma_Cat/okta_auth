﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Primitives;
using AspNet.Security.OpenIdConnect.Server;
using GlobalLoop.Authorization.Requests;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Okta.Sdk;
using Okta.Sdk.Configuration;

namespace GlobalLoop.Authorization.WebApi
{
    public partial class Startup
    {
        private void AddAuth(IServiceCollection services)
        {
            services.AddSingleton(Configuration.GetSection("OktaClientConfiguration").Get<OktaClientConfiguration>());
            services.AddHttpClient<IOktaClient, OktaClient>();

            var key = new SymmetricSecurityKey(Convert.FromBase64String(Configuration["AuthSigningKeyBase64"]));

            services
                .AddAuthentication(options => Configuration.Bind("AuthenticationOptions", options))
                .AddOpenIdConnectServer(options =>
                {
                    Configuration.Bind("OpenIdConnectServerOptions", options);
                    LoggerFactory.CreateLogger<OpenIdConnectServerOptions>().LogInformation($"Issuer = {options.Issuer}");
                    options.SigningCredentials.AddKey(key);
                    options.AccessTokenHandler = new JwtSecurityTokenHandler
                    {
                        OutboundClaimTypeMap = new Dictionary<string, string>()
                    };
                    options.Provider.OnValidateTokenRequest = context =>
                    {
                        if (!context.Request.IsPasswordGrantType() && !context.Request.IsRefreshTokenGrantType())
                        {
                            context.Reject(OpenIdConnectConstants.Errors.UnsupportedGrantType, "Only grant_type=password and refresh_token requests are accepted by this server.");
                            return Task.CompletedTask;
                        }

                        if (string.IsNullOrEmpty(context.ClientId))
                        {
                            context.Skip();
                            return Task.CompletedTask;
                        }

                        // Note: to mitigate brute force attacks, you SHOULD strongly consider applying
                        // a key derivation function like PBKDF2 to slow down the secret validation process.
                        // You SHOULD also consider using a time-constant comparer to prevent timing attacks.
                        // if (isValid(context.ClientId, context.ClientSecret))
                        // {
                        //     context.Validate();
                        // }

                        // Note: if Validate() is not explicitly called, the request is automatically rejected.
                        return Task.CompletedTask;
                    };

                    // Implement OnHandleTokenRequest to support token requests.
                    options.Provider.OnHandleTokenRequest = async context =>
                    {
                        await context.HttpContext.RequestServices.GetService<IMediator>().Send(new HandleTokenRequest.Request { Context = context });
                    };
                })
                .AddJwtBearer(options =>
                {
                    Configuration.Bind("JwtBearerOptions", options);
                    options.TokenValidationParameters.IssuerSigningKey = key;
                });
        }
    }
}