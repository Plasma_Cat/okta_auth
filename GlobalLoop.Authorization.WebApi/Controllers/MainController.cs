﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Threading.Tasks;
using GlobalLoop.Authorization.Requests;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GlobalLoop.Authorization.WebApi.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MainController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MainController(IMediator mediator)
            => _mediator = mediator;

        [AllowAnonymous]
        [HttpGet("ping")]
        public string Ping() => "pong";

        [HttpPost("")]
        public async Task<UserFullDto> AddUser([FromBody]AddUser.Request request)
            => await _mediator.Send(request, HttpContext.RequestAborted);

        [HttpGet("{Id}")]
        public async Task<GetUserDetails.Response> GetUserDetails([FromRoute]GetUserDetails.Request request)
            => await _mediator.Send(request, HttpContext.RequestAborted);

        [HttpPut("")]
        public async Task<UserFullDto> EditUser([FromBody]EditUser.Request request)
            => await _mediator.Send(request, HttpContext.RequestAborted);

        [HttpPut("status")]
        public async Task SetStatus([FromBody]SetStatus.Request request)
            => await _mediator.Send(request, HttpContext.RequestAborted);

        [HttpPut("password")]
        public async Task ChangePassword([FromBody]ChangePassword.Request request)
            => await _mediator.Send(request, HttpContext.RequestAborted);

        [HttpPost("synchronize")]
        public async Task Synchronize()
            => await _mediator.Send(new Synchronize.Request(), HttpContext.RequestAborted);
    }
}