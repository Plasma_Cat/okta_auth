﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace GlobalLoop.Authorization
{
    public static class OktaOptionsExtensions
    {
        public static void ValidateIntegrationEnabled(this IOptionsMonitor<OktaOptions> optionsMonitor, ILogger logger, bool isIntegrationEnable = true)
        {
            if (optionsMonitor.CurrentValue.EnableIntegration != isIntegrationEnable)
            {
                logger.LogCritical($"Okta {nameof(OktaOptions.EnableIntegration)} = {!isIntegrationEnable}.");
                throw new GlobalLoopAuthorizationException(10004, !isIntegrationEnable);
            }
        }
    }
}