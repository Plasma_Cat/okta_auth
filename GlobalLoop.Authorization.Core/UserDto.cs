﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

namespace GlobalLoop.Authorization
{
    public class UserDto : UserBaseDto
    {
        public int Id { get; set; }
    }
}