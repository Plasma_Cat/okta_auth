﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

namespace GlobalLoop.Authorization
{
    public class UserBaseDto
    {
        public string UserName { get; set; }

        public IdentityName IdentityName { get; set; }

        public string Email { get; set; }
    }
}