﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System;
using System.Runtime.Serialization;
using GlobalLoop.Authorization.Resources;

namespace GlobalLoop.Authorization
{
    [Serializable]
    public class GlobalLoopAuthorizationException : GlobalLoopException
    {
        public GlobalLoopAuthorizationException()
        {
        }

        public GlobalLoopAuthorizationException(string message)
            : base(message)
        {
        }

        public GlobalLoopAuthorizationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public GlobalLoopAuthorizationException(int errorNumber, params object[] messageArgs)
            : this(null, errorNumber, messageArgs)
        {
        }

        public GlobalLoopAuthorizationException(Exception innerException, int errorNumber, params object[] messageArgs)
            : base(Errors.ResourceManager, errorNumber => $"BSA{errorNumber:00000}", innerException, errorNumber, messageArgs)
        {
        }

        protected GlobalLoopAuthorizationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public static GlobalLoopAuthorizationException UserNotFound(int userId)
            => new GlobalLoopAuthorizationException(20001, userId);

        public static GlobalLoopAuthorizationException UserNotFound(string username)
            => new GlobalLoopAuthorizationException(20001, username);
    }
}