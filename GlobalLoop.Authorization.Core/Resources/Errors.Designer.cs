﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalLoop.Authorization.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Errors {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Errors() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("GlobalLoop.Authorization.Resources.Errors", typeof(Errors).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User with same UserName already exist in Okta..
        /// </summary>
        internal static string BSA10001 {
            get {
                return ResourceManager.GetString("BSA10001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Okta user &apos;{0}&apos;: organization undefined..
        /// </summary>
        internal static string BSA10002 {
            get {
                return ResourceManager.GetString("BSA10002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User &apos;{0}&apos; has different oktaId..
        /// </summary>
        internal static string BSA10003 {
            get {
                return ResourceManager.GetString("BSA10003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Okta EnableIntegration = {0}..
        /// </summary>
        internal static string BSA10004 {
            get {
                return ResourceManager.GetString("BSA10004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid user credentials..
        /// </summary>
        internal static string BSA10005 {
            get {
                return ResourceManager.GetString("BSA10005", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User {0} not found..
        /// </summary>
        internal static string BSA20001 {
            get {
                return ResourceManager.GetString("BSA20001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User {0} has no OktaId..
        /// </summary>
        internal static string BSA20002 {
            get {
                return ResourceManager.GetString("BSA20002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User with same UserName already exist..
        /// </summary>
        internal static string BSA20003 {
            get {
                return ResourceManager.GetString("BSA20003", resourceCulture);
            }
        }
    }
}
