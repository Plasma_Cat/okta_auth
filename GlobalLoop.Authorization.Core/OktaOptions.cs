﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

namespace GlobalLoop.Authorization
{
    public class OktaOptions
    {
        public bool EnableIntegration { get; set; }
    }
}