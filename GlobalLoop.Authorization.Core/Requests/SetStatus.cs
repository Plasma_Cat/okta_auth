﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Diagnostics.CodeAnalysis;
using MediatR;

namespace GlobalLoop.Authorization.Requests
{
    [ExcludeFromCodeCoverage]
    public static class SetStatus
    {
        public sealed class Request : IRequest
        {
            public int Id { get; set; }

            public bool IsDisabled { get; set; }
        }
    }
}