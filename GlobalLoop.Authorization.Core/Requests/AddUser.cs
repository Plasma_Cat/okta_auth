﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Diagnostics.CodeAnalysis;
using MediatR;

namespace GlobalLoop.Authorization.Requests
{
    [ExcludeFromCodeCoverage]
    public static class AddUser
    {
        public sealed class Request : UserBaseDto, IRequest<UserFullDto>
        {
            public string Password { get; set; }
        }
    }
}