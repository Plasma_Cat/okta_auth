﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System;
using System.Diagnostics.CodeAnalysis;
using MediatR;

namespace GlobalLoop.Authorization.Requests
{
    [ExcludeFromCodeCoverage]
    public static class GetUserDetails
    {
        public sealed class Request : IRequest<Response>
        {
            public int Id { get; set; }
        }

        public sealed class Response
        {
            public string Status { get; set; }

            public string Email { get; set; }

            public string PrimaryPhone { get; set; }

            public string MobilePhone { get; set; }

            public string Office { get; set; }

            public string City { get; set; }

            public string CountryCode { get; set; }

            public DateTimeOffset? LastLogin { get; set; }
        }
    }
}