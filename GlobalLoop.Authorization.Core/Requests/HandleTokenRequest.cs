﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Diagnostics.CodeAnalysis;
using AspNet.Security.OpenIdConnect.Server;
using MediatR;

namespace GlobalLoop.Authorization.Requests
{
    [ExcludeFromCodeCoverage]
    public static class HandleTokenRequest
    {
        public sealed class Request : IRequest
        {
            public HandleTokenRequestContext Context { get; set; }
        }
    }
}