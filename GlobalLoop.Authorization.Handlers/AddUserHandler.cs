﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GlobalLoop.Authorization.Data;
using GlobalLoop.Authorization.Requests;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Npgsql;
using Okta.Sdk;
using static GlobalLoop.Authorization.Handlers.HandlersDefaults;

namespace GlobalLoop.Authorization.Handlers
{
    internal sealed class AddUserHandler : IRequestHandler<AddUser.Request, UserFullDto>
    {
        private readonly AuthorizationDbContext _db;
        private readonly IOktaClient _oktaClient;
        private readonly IMapper _mapper;
        private readonly IRequestContextAccessor _requestContext;
        private readonly OktaOptions _oktaOptions;
        private readonly IPasswordHasher<Data.Entities.User> _passwordHasher;

        public AddUserHandler(
            AuthorizationDbContext db,
            IOktaClient oktaClient,
            IMapper mapper,
            IRequestContextAccessor requestContextAccessor,
            IOptionsMonitor<OktaOptions> oktaOptionsMonitor,
            IPasswordHasher<Data.Entities.User> passwordHasher)
        {
            _db = db;
            _oktaClient = oktaClient;
            _mapper = mapper;
            _requestContext = requestContextAccessor;
            _oktaOptions = oktaOptionsMonitor.CurrentValue;
            _passwordHasher = passwordHasher;
        }

        public async Task<UserFullDto> Handle(AddUser.Request request, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<Data.Entities.User>(request);
            user.PasswordHash = _passwordHasher.HashPassword(user, request.Password);
            using var transaction = await _db.Database.BeginTransactionAsync(cancellationToken);
            try
            {
                await _db.AddAsync(user, cancellationToken);
                await _db.SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateException e) when (e.InnerException is PostgresException pe && pe.SqlState == "23505")
            {
                throw new GlobalLoopAuthorizationException(e, 20003);
            }

            if (_oktaOptions.EnableIntegration)
            {
                var oktaUserProfile = _mapper.Map<UserProfile>(request);
                try
                {
                    var oktaUser = await _oktaClient.Users.CreateUserAsync(
                        new CreateUserWithPasswordOptions
                        {
                            Activate = true,
                            Profile = oktaUserProfile,
                            Password = request.Password
                        }, cancellationToken);
                    user.OktaId = oktaUser.Id;
                }
                catch (OktaApiException e) when (e.ErrorCode.Equals(OktaErrorCodes.ObjectWithThisFieldAlreadyExists))
                {
                    throw new GlobalLoopAuthorizationException(e, 10001);
                }

                _db.Update(user);
            }

            await _db.SaveChangesAsync();
            var result = _mapper.Map<UserFullDto>(user);
            await transaction.CommitAsync();
            return result;
        }
    }
}