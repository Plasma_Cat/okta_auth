﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Threading;
using System.Threading.Tasks;
using GlobalLoop.Authorization.Data;
using GlobalLoop.Authorization.Requests;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace GlobalLoop.Authorization.Handlers
{
    internal sealed class ChangePasswordHandler : IRequestHandler<ChangePassword.Request>
    {
        private readonly AuthorizationDbContext _db;
        private readonly IRequestContextAccessor _requestContext;
        private readonly IPasswordHasher<Data.Entities.User> _passwordHasher;

        public ChangePasswordHandler(
            AuthorizationDbContext db,
            IRequestContextAccessor requestContextAccessor,
            IOptionsMonitor<OktaOptions> oktaOptionsMonitor,
            IPasswordHasher<Data.Entities.User> passwordHasher,
            ILogger<ChangePasswordHandler> logger)
        {
            oktaOptionsMonitor.ValidateIntegrationEnabled(logger, false);
            _db = db;
            _requestContext = requestContextAccessor;
            _passwordHasher = passwordHasher;
        }

        public async Task<Unit> Handle(ChangePassword.Request request, CancellationToken cancellationToken)
        {
            var user = await _db.Users.SingleOrDefaultAsync(x => x.Id == _requestContext.User.Id, cancellationToken);
            if (_passwordHasher.VerifyHashedPassword(user, user.PasswordHash, request.OldPassword) == PasswordVerificationResult.Failed)
                throw new GlobalLoopAuthorizationException(10005);

            user.PasswordHash = _passwordHasher.HashPassword(user, request.NewPassword);
            _db.Update(user);
            await _db.SaveChangesAsync();
            return Unit.Value;
        }
    }
}