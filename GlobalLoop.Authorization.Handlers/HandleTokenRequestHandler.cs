﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using GlobalLoop.AspNetCore;
using GlobalLoop.Authorization.Data;
using GlobalLoop.Authorization.Data.Entities;
using GlobalLoop.Authorization.Requests;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Okta.Sdk;
using static GlobalLoop.Authorization.Handlers.HandlersDefaults;

namespace GlobalLoop.Authorization.Handlers
{
    internal sealed class HandleTokenRequestHandler : IRequestHandler<HandleTokenRequest.Request>
    {
        private readonly IOktaClient _oktaClient;
        private readonly AuthorizationDbContext _db;
        private readonly OktaOptions _oktaOptions;
        private readonly IPasswordHasher<Data.Entities.User> _passwordHasher;

        public HandleTokenRequestHandler(IOktaClient oktaClient, AuthorizationDbContext db, IOptionsMonitor<OktaOptions> oktaOptionsMonitor, IPasswordHasher<Data.Entities.User> passwordHasher)
        {
            _oktaClient = oktaClient;
            _db = db;
            _oktaOptions = oktaOptionsMonitor.CurrentValue;
            _passwordHasher = passwordHasher;
        }

        public async Task<Unit> Handle(HandleTokenRequest.Request request, CancellationToken cancellationToken)
        {
            string userName = null;
            string oktaId = null;
            Data.Entities.User dbuser = null;
            switch (request.Context.Request.GrantType)
            {
                case "password" when _oktaOptions.EnableIntegration:
                    try
                    {
                        var resource = await _oktaClient.PostAsync<Resource>("api/v1/authn", new
                        {
                            username = request.Context.Request.Username,
                            password = request.Context.Request.Password
                        });
                        oktaId = resource.GetProperty<Resource>("_embedded").GetProperty<Okta.Sdk.User>("user").Id;
                    }
                    catch (OktaApiException e) when (e.ErrorCode == OktaErrorCodes.AuthenticationFailed)
                    {
                        request.Context.Reject(OpenIdConnectConstants.Errors.InvalidGrant, "Invalid user credentials.");
                        return Unit.Value;
                    }

                    break;
                case "password" when !_oktaOptions.EnableIntegration:
                    dbuser = await _db.Users
                        .SingleOrDefaultAsync(x => x.UserName.ToUpper() == request.Context.Request.Username.ToUpper() && !x.IsDisabled);
                    if (dbuser == null || dbuser.PasswordHash == null || _passwordHasher.VerifyHashedPassword(dbuser, dbuser.PasswordHash, request.Context.Request.Password) == PasswordVerificationResult.Failed)
                    {
                        request.Context.Reject(OpenIdConnectConstants.Errors.InvalidGrant, "Invalid user credentials.");
                        return Unit.Value;
                    }

                    break;
                case "refresh_token":
                    userName = request.Context.Ticket.Principal.GetClaim(AspNetCoreDefaults.ClaimTypes.UserName);
                    oktaId = request.Context.Ticket.Principal.GetClaim(HandlersDefaults.ClaimTypes.OktaId);
                    if (oktaId == null && _oktaOptions.EnableIntegration)
                    {
                        request.Context.Reject(OpenIdConnectConstants.Errors.InvalidToken, $"Missed {HandlersDefaults.ClaimTypes.OktaId} claim.");
                        return Unit.Value;
                    }

                    break;
                default:
                    request.Context.Reject(OpenIdConnectConstants.Errors.InvalidGrant, "Only grant_type=password and refresh_token requests are accepted by this server.");
                    return Unit.Value;
            }

            userName ??= request.Context.Request.Username;
            var identity = new ClaimsIdentity(
                request.Context.Scheme.Name,
                AspNetCoreDefaults.ClaimTypes.FirstName,
                HandlersDefaults.ClaimTypes.Role);
            if (_oktaOptions.EnableIntegration)
            {
                IUser oktaUser = null;
                dbuser = await _db.Users
                    .SingleOrDefaultAsync(x => x.OktaId == oktaId && !x.IsDisabled);
                if (dbuser == null)
                {
                    var existedUser = await _db.Users.FirstOrDefaultAsync(x => x.OktaId.Equals(oktaId));
                    if (existedUser != null)
                    {
                        request.Context.Reject(OpenIdConnectConstants.Errors.AccessDenied, existedUser.IsDisabled ? "User is disabled." : "User not found.");
                        return Unit.Value;
                    }

                    oktaUser = await _oktaClient.Users.GetUserAsync(oktaId);
                    dbuser = await _db.Users
                        .FirstOrDefaultAsync(x => x.UserName.ToUpper() == userName.ToUpper());
                    if (dbuser == null)
                    {
                        dbuser = new Data.Entities.User
                        {
                            OktaId = oktaId,
                            UserName = userName,
                            FirstName = oktaUser.Profile.FirstName,
                            LastName = oktaUser.Profile.LastName,
                            IsDisabled = false,
                        };
                    }
                    else if (dbuser.OktaId == null)
                    {
                        dbuser.OktaId = oktaId;
                    }
                    else // oktaId != user.OktaId
                    {
                        request.Context.Reject(OpenIdConnectConstants.Errors.ServerError, "Invalid OktaId.");
                        return Unit.Value;
                    }
                }

                dbuser.PasswordHash = _passwordHasher.HashPassword(dbuser, request.Context.Request.Password);
                if (dbuser.Id == default)
                {
                    _db.Add(dbuser);
                }
                else
                {
                    _db.Update(dbuser);
                }

                await _db.SaveChangesAsync();

                oktaUser ??= await _oktaClient.Users.GetUserAsync(oktaId);
                identity.AddClaim(HandlersDefaults.ClaimTypes.OktaId, oktaId, OpenIdConnectConstants.Destinations.AccessToken);
                identity.AddClaim(HandlersDefaults.ClaimTypes.Email, oktaUser.Profile.Email, OpenIdConnectConstants.Destinations.AccessToken);
                identity.AddClaim(AspNetCoreDefaults.ClaimTypes.FirstName, oktaUser.Profile.FirstName, OpenIdConnectConstants.Destinations.AccessToken);
                identity.AddClaim(AspNetCoreDefaults.ClaimTypes.LastName, oktaUser.Profile.LastName, OpenIdConnectConstants.Destinations.AccessToken);
            }
            else
            {
                identity.AddClaim(AspNetCoreDefaults.ClaimTypes.FirstName, dbuser.FirstName, OpenIdConnectConstants.Destinations.AccessToken);
                identity.AddClaim(AspNetCoreDefaults.ClaimTypes.LastName, dbuser.LastName, OpenIdConnectConstants.Destinations.AccessToken);
            }

            identity.AddClaim(new Claim(
                HandlersDefaults.ClaimTypes.Subject,
                dbuser.Id.ToString(),
                ClaimValueTypes.Integer32));

            identity.AddClaim(new Claim(
                HandlersDefaults.ClaimTypes.IsPortalAdmin,
                dbuser.IsAdmin.ToString(),
                ClaimValueTypes.Boolean)
                .SetDestinations(OpenIdConnectConstants.Destinations.AccessToken));

            identity.AddClaim(AspNetCoreDefaults.ClaimTypes.UserName, userName, OpenIdConnectConstants.Destinations.AccessToken);

            var ticket = new AuthenticationTicket(
                new ClaimsPrincipal(identity),
                request.Context.Scheme.Name);

            // Call SetScopes with the list of scopes you want to grant (specify offline_access to issue a refresh token).
            ticket.SetScopes(OpenIdConnectConstants.Scopes.OfflineAccess);
            request.Context.Validate(ticket);
            return Unit.Value;
        }
    }
}