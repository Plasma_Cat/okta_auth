﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using AutoMapper;
using Okta.Sdk;

namespace GlobalLoop.Authorization.Handlers
{
    public class OktaProfile : Profile
    {
        public OktaProfile()
        {
            CreateMap<UserBaseDto, UserProfile>(MemberList.Source)
                .ForMember(m => m.Login, cfg => cfg.MapFrom(x => x.UserName))
                .ForMember(m => m.FirstName, cfg => cfg.MapFrom(x => x.IdentityName.FirstName))
                .ForMember(m => m.LastName, cfg => cfg.MapFrom(x => x.IdentityName.LastName))
                .ForMember(m => m.Email, cfg => cfg.MapFrom(x => x.Email));
        }
    }
}