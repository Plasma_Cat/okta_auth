﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using AspNet.Security.OpenIdConnect.Primitives;

namespace GlobalLoop.Authorization.Handlers
{
    public static class HandlersDefaults
    {
        public static class ClaimTypes
        {
            public const string OktaId = "okta_id";
            public const string AvatarUrl = OpenIdConnectConstants.Claims.Picture;
            public const string IsPortalAdmin = "is_portal_admin";
            public const string Subject = OpenIdConnectConstants.Claims.Subject;
            public const string Role = OpenIdConnectConstants.Claims.Role;
            public const string Email = OpenIdConnectConstants.Claims.Email;
        }

        internal static class OktaErrorCodes
        {
            public const string AuthenticationFailed = "E0000004";
            public const string ResourceNotFound = "E0000007";
            public const string ObjectWithThisFieldAlreadyExists = "E0000001";
        }
    }
}