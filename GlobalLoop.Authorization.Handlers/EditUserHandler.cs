﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GlobalLoop.Authorization.Data;
using GlobalLoop.Authorization.Requests;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Npgsql;
using Okta.Sdk;
using static GlobalLoop.Authorization.Handlers.HandlersDefaults;

namespace GlobalLoop.Authorization.Handlers
{
    internal sealed class EditUserHandler : IRequestHandler<EditUser.Request, UserFullDto>
    {
        private readonly AuthorizationDbContext _db;
        private readonly IOktaClient _oktaClient;
        private readonly IMapper _mapper;
        private readonly ILogger<EditUserHandler> _logger;
        private readonly IRequestContextAccessor _requestContext;
        private readonly OktaOptions _oktaOptions;

        public EditUserHandler(AuthorizationDbContext db, IOktaClient oktaClient, IMapper mapper, ILogger<EditUserHandler> logger, IRequestContextAccessor requestContextAccessor, IOptionsMonitor<OktaOptions> oktaOptionsMonitor)
        {
            _db = db;
            _oktaClient = oktaClient;
            _mapper = mapper;
            _logger = logger;
            _requestContext = requestContextAccessor;
            _oktaOptions = oktaOptionsMonitor.CurrentValue;
        }

        public async Task<UserFullDto> Handle(EditUser.Request request, CancellationToken cancellationToken)
        {
            var user = await _db.Users.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (user == null)
                throw GlobalLoopAuthorizationException.UserNotFound(request.Id);

            if (_oktaOptions.EnableIntegration)
            {
                user.ValidateOktaIdIsNotNull(_logger);
            }

            user.UserName = request.UserName;
            user.FirstName = request.IdentityName.FirstName;
            user.LastName = request.IdentityName.LastName;
            using var transaction = await _db.Database.BeginTransactionAsync(cancellationToken);
            try
            {
                _db.Update(user);
                await _db.SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateException e) when (e.InnerException is PostgresException pe && pe.SqlState == "23505")
            {
                throw new GlobalLoopAuthorizationException(e, 20003);
            }

            if (_oktaOptions.EnableIntegration)
            {
                IUser oktaUser;
                try
                {
                    oktaUser = await _oktaClient.Users.GetUserAsync(user.OktaId, cancellationToken);
                }
                catch (OktaApiException e) when (e.ErrorCode.Equals(OktaErrorCodes.ResourceNotFound))
                {
                    _logger.LogCritical(e, $"User {{id:{request.Id}, oktaId:'{user.OktaId}'}} not found in Okta.");
                    throw;
                }

                oktaUser.Profile = _mapper.Map<UserProfile>(request);
                await _oktaClient.Users.UpdateUserAsync(oktaUser, user.OktaId, cancellationToken);
            }

            var result = _mapper.Map<UserFullDto>(user);
            await transaction.CommitAsync();
            return result;
        }
    }
}