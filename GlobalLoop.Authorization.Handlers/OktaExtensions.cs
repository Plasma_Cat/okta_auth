﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using Okta.Sdk;

namespace GlobalLoop.Authorization.Handlers
{
    public static class OktaExtensions
    {
        public static string GetOrganization(this IUser user)
            => user.Profile.GetProperty<string>("organization");
    }
}