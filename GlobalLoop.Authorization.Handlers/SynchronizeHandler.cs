﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GlobalLoop.Authorization.Data;
using GlobalLoop.Authorization.Data.Entities;
using GlobalLoop.Authorization.Requests;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Okta.Sdk;

namespace GlobalLoop.Authorization.Handlers
{
    internal sealed class SynchronizeHandler : IRequestHandler<Synchronize.Request>
    {
        private readonly AuthorizationDbContext _db;
        private readonly IOktaClient _oktaClient;
        private readonly ILogger<SynchronizeHandler> _logger;

        public SynchronizeHandler(AuthorizationDbContext db, IOktaClient oktaClient, ILogger<SynchronizeHandler> logger, IOptionsMonitor<OktaOptions> oktaOptionsMonitor)
        {
            oktaOptionsMonitor.ValidateIntegrationEnabled(logger);
            _db = db;
            _oktaClient = oktaClient;
            _logger = logger;
        }

        public async Task<Unit> Handle(Synchronize.Request request, CancellationToken cancellationToken)
        {
            var oktaUsers = new List<IUser>();
            await _oktaClient.Users.ForEachAsync(
                oktaUser =>
            {
                oktaUsers.Add(oktaUser);
            }, cancellationToken).ConfigureAwait(false);

            foreach (var oktaUser in oktaUsers)
            {
                cancellationToken.ThrowIfCancellationRequested();
                _logger.LogInformation($"Sync okta user '{oktaUser.Id}'.");
                try
                {
                    await ProcessUserAsync(oktaUser, cancellationToken);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, $"Okta user '{oktaUser.Id}': error sync.");
                }
            }

            _db.UpdateRange(_db.Users.Where(x => !oktaUsers.Select(u => u.Id).Contains(x.OktaId))
                .Select(x => new Data.Entities.User
                {
                    Id = x.Id,
                    OktaId = null,
                    IsDisabled = true
                }));
            await _db.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }

        private async Task ProcessUserAsync(IUser oktaUser, CancellationToken cancellationToken)
        {
            using var transaction = await _db.Database.BeginTransactionAsync(cancellationToken);
            var isDisabled = oktaUser.Status != UserStatus.Active;
            var user = await _db.Users.FirstOrDefaultAsync(x => x.OktaId == oktaUser.Id, cancellationToken);
            if (user == null)
            {
                user = await _db.Users.FirstOrDefaultAsync(x => x.UserName.ToUpper() == oktaUser.Profile.Login.ToUpper(), cancellationToken);
                if (user == null)
                {
                    user = new Data.Entities.User
                    {
                        OktaId = oktaUser.Id,
                        UserName = oktaUser.Profile.Login,
                        FirstName = oktaUser.Profile.FirstName,
                        LastName = oktaUser.Profile.LastName,
                        IsDisabled = isDisabled
                    };
                    await _db.AddAsync(user, cancellationToken);
                }
                else if (user.OktaId == null)
                {
                    user.UserName = oktaUser.Profile.Login;
                    _db.Update(user);
                }
                else
                {
                    throw new GlobalLoopAuthorizationException(10003, user.UserName);
                }

                await _db.SaveChangesAsync(cancellationToken);
            }

            if (isDisabled != user.IsDisabled)
            {
                user.IsDisabled = isDisabled;
                _db.Update(user);
                await _db.SaveChangesAsync(cancellationToken);
            }

            var organization = oktaUser.GetOrganization();
            if (string.IsNullOrWhiteSpace(organization))
                throw new GlobalLoopAuthorizationException(10002, oktaUser.Id);

            await transaction.CommitAsync();
        }
    }
}