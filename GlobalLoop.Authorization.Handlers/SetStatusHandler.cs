﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Threading;
using System.Threading.Tasks;
using GlobalLoop.Authorization.Data;
using GlobalLoop.Authorization.Requests;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Okta.Sdk;

namespace GlobalLoop.Authorization.Handlers
{
    internal sealed class SetStatusHandler : IRequestHandler<SetStatus.Request>
    {
        private readonly AuthorizationDbContext _db;
        private readonly IOktaClient _oktaClient;
        private readonly ILogger<SetStatusHandler> _logger;
        private readonly IRequestContextAccessor _requestContext;
        private readonly OktaOptions _oktaOptions;

        public SetStatusHandler(AuthorizationDbContext db, IOktaClient oktaClient, ILogger<SetStatusHandler> logger, IRequestContextAccessor requestContextAccessor, IOptionsMonitor<OktaOptions> oktaOptionsMonitor)
        {
            _db = db;
            _oktaClient = oktaClient;
            _logger = logger;
            _requestContext = requestContextAccessor;
            _oktaOptions = oktaOptionsMonitor.CurrentValue;
        }

        public async Task<Unit> Handle(SetStatus.Request request, CancellationToken cancellationToken)
        {
            using var transaction = await _db.Database.BeginTransactionAsync(cancellationToken);
            var user = await _db.Users.FirstOrDefaultAsync(
                x => x.Id == request.Id,
                cancellationToken);
            if (user == null)
                throw GlobalLoopAuthorizationException.UserNotFound(request.Id);

            user.ValidateOktaIdIsNotNull(_logger);

            if (user.IsDisabled == request.IsDisabled)
                return Unit.Value;

            user.IsDisabled = request.IsDisabled;
            _db.Update(user);
            await _db.SaveChangesAsync(cancellationToken);
            if (_oktaOptions.EnableIntegration)
            {
                var oktaUser = await _oktaClient.Users.GetUserAsync(user.OktaId, cancellationToken);
                if (request.IsDisabled && oktaUser.Status != UserStatus.Suspended)
                {
                    await _oktaClient.Users.SuspendUserAsync(user.OktaId, cancellationToken);
                }
                else if (!request.IsDisabled && oktaUser.Status == UserStatus.Suspended)
                {
                    await _oktaClient.Users.UnsuspendUserAsync(user.OktaId, cancellationToken);
                }
            }

            await transaction.CommitAsync();
            _logger.LogInformation($"User {request.Id} status changed: {nameof(user.IsDisabled)}={request.IsDisabled}.");
            return Unit.Value;
        }
    }
}