﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Threading;
using System.Threading.Tasks;
using GlobalLoop.Authorization.Data;
using GlobalLoop.Authorization.Requests;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Okta.Sdk;

namespace GlobalLoop.Authorization.Handlers
{
    internal sealed class GetUserDetailsHandler : IRequestHandler<GetUserDetails.Request, GetUserDetails.Response>
    {
        private readonly AuthorizationDbContext _db;
        private readonly IOktaClient _oktaClient;
        private readonly ILogger<GetUserDetailsHandler> _logger;

        public GetUserDetailsHandler(AuthorizationDbContext db, IOktaClient oktaClient, ILogger<GetUserDetailsHandler> logger, IOptionsMonitor<OktaOptions> oktaOptionsMonitor)
        {
            oktaOptionsMonitor.ValidateIntegrationEnabled(logger);
            _db = db;
            _oktaClient = oktaClient;
            _logger = logger;
        }

        public async Task<GetUserDetails.Response> Handle(GetUserDetails.Request request, CancellationToken cancellationToken)
        {
            var user = await _db.Users.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (user == null)
                throw GlobalLoopAuthorizationException.UserNotFound(request.Id);

            user.ValidateOktaIdIsNotNull(_logger);

            var oktaUser = await _oktaClient.Users.GetUserAsync(user.OktaId, cancellationToken);
            var profileData = oktaUser.Profile.GetData();
            return new GetUserDetails.Response
            {
                Status = oktaUser.Status?.Value,
                Email = oktaUser.Profile.Email,
                PrimaryPhone = profileData.TryGetValue("primaryPhone", out var primaryPhone) ? primaryPhone.ToString() : null,
                MobilePhone = oktaUser.Profile.MobilePhone,
                Office = profileData.TryGetValue("streetAddress", out var streetAddress) ? streetAddress.ToString() : null,
                City = profileData.TryGetValue("city", out var city) ? city.ToString() : null,
                CountryCode = profileData.TryGetValue("countryCode", out var countryCode) ? countryCode.ToString() : null,
                LastLogin = oktaUser.LastLogin
            };
        }
    }
}