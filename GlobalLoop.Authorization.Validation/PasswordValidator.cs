﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System.Text.RegularExpressions;
using FluentValidation.Validators;

namespace GlobalLoop.Authorization.Validation
{
    internal sealed class PasswordValidator : PropertyValidator
    {
        private const int MinLength = 8;
        private static Regex _passwordRegex = new Regex(@"\S+", RegexOptions.Compiled);

        public PasswordValidator()
            : base("Invalid password format.")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var password = (string)context.PropertyValue;
            return password.Length >= MinLength && _passwordRegex.IsMatch(password);
        }
    }
}