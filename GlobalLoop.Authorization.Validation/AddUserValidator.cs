﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using FluentValidation;
using GlobalLoop.Authorization.Requests;
using GlobalLoop.Authorization.Validation.Requests;

namespace GlobalLoop.Authorization.Validation
{
    public class AddUserValidator : UserBaseDtoValidator<AddUser.Request>
    {
        public AddUserValidator()
        {
            RuleFor(x => x.Password)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull()
                .NotEmpty()
                .Password();
        }
    }
}