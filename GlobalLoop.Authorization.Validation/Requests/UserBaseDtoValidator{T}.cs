﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using FluentValidation;

namespace GlobalLoop.Authorization.Validation.Requests
{
    public class UserBaseDtoValidator<T> : AbstractValidator<T>
        where T : UserBaseDto
    {
        public UserBaseDtoValidator()
        {
            RuleFor(x => x.UserName).Equal(x => x.Email);
            RuleFor(x => x.IdentityName).NotNull();
            RuleFor(x => x.Email).NotNull().EmailAddress();
        }
    }
}