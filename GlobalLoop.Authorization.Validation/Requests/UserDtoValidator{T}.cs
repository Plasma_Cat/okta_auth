﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using FluentValidation;

namespace GlobalLoop.Authorization.Validation.Requests
{
    public class UserDtoValidator<T> : UserBaseDtoValidator<T>
        where T : UserDto
    {
        public UserDtoValidator()
        {
            RuleFor(x => x.Id).GreaterThan(0);
        }
    }
}