﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using FluentValidation;
using GlobalLoop.Authorization.Requests;

namespace GlobalLoop.Authorization.Validation
{
    public class GetUserDetailsValidator : AbstractValidator<GetUserDetails.Request>
    {
        public GetUserDetailsValidator()
        {
            RuleFor(x => x.Id).GreaterThan(0);
        }
    }
}