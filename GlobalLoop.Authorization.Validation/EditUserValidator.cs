﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using GlobalLoop.Authorization.Requests;
using GlobalLoop.Authorization.Validation.Requests;

namespace GlobalLoop.Authorization.Validation
{
    public class EditUserValidator : UserDtoValidator<EditUser.Request>
    {
    }
}