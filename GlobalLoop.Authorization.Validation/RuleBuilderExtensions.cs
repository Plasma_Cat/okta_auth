﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using FluentValidation;

namespace GlobalLoop.Authorization.Validation
{
    public static class RuleBuilderExtensions
    {
        public static IRuleBuilderOptions<T, string> Password<T>(this IRuleBuilder<T, string> ruleBuilder)
            => ruleBuilder.SetValidator(new PasswordValidator());
    }
}