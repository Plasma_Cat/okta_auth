﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using FluentValidation;
using GlobalLoop.Authorization.Requests;

namespace GlobalLoop.Authorization.Validation
{
    public class ChangePasswordValidator : AbstractValidator<ChangePassword.Request>
    {
        public ChangePasswordValidator()
        {
            RuleFor(x => x.OldPassword)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.NewPassword)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull()
                .NotEmpty()
                .Password();
        }
    }
}