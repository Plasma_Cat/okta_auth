﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GlobalLoop.Authorization.Data.Migrations
{
    public partial class amk_okta_users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    username = table.Column<string>(type: "varchar(200)", nullable: false),
                    first_name = table.Column<string>(type: "varchar(200)", nullable: false),
                    last_name = table.Column<string>(type: "varchar(200)", nullable: false),
                    is_disabled = table.Column<bool>(nullable: false),
                    okta_id = table.Column<string>(type: "varchar(100)", nullable: true),
                    is_admin = table.Column<bool>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: true),
                    password_hash = table.Column<string>(type: "varchar(200)", nullable: false),
                    created = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    quota_agents = table.Column<int>(type: "int", nullable: false),
                    quota_senarios_per_agent = table.Column<int>(type: "int", nullable: false),
                    token = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("users_pk", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "users_ui1",
                table: "users",
                column: "username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "users");
        }
    }
}
