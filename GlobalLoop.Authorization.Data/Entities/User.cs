﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using System;

namespace GlobalLoop.Authorization.Data.Entities
{
    public class User
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool IsDisabled { get; set; }

        public string OktaId { get; set; }

        public bool IsAdmin { get; set; }

        public string PhoneNumber { get; set; }

        public string PasswordHash { get; set; }

        public DateTime Created { get; set; }

        // TODO: Remove this fields later
        public int QuotaAgents { get; set; }

        public int QuotaSenariosPerAgent { get; set; }

        public string Token { get; set; }
    }
}