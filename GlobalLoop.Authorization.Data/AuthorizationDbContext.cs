﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using GlobalLoop.Authorization.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace GlobalLoop.Authorization.Data
{
    public class AuthorizationDbContext : DbContext
    {
        public AuthorizationDbContext(DbContextOptions<AuthorizationDbContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureUserEntity(modelBuilder);
        }

        private void ConfigureUserEntity(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(e =>
            {
                e.Property(x => x.Id).HasColumnName("id").IsRequired().UseIdentityColumn();
                e.Property(x => x.UserName).HasColumnName("username").IsRequired().HasColumnType("varchar(200)");
                e.Property(x => x.FirstName).HasColumnName("first_name").IsRequired().HasColumnType("varchar(200)");
                e.Property(x => x.LastName).HasColumnName("last_name").IsRequired().HasColumnType("varchar(200)");
                e.Property(x => x.IsDisabled).HasColumnName("is_disabled").IsRequired();
                e.Property(x => x.IsAdmin).HasColumnName("is_admin").IsRequired();
                e.Property(x => x.OktaId).HasColumnName("okta_id").HasColumnType("varchar(100)");
                e.Property(x => x.PasswordHash).HasColumnName("password_hash").IsRequired().HasColumnType("varchar(200)");
                e.Property(x => x.Created).HasColumnName("created").IsRequired().HasDefaultValueSql("GetDate()");

                e.Property(x => x.QuotaAgents).HasColumnName("quota_agents").HasColumnType("int");
                e.Property(x => x.QuotaSenariosPerAgent).HasColumnName("quota_senarios_per_agent").HasColumnType("int");
                e.Property(x => x.Token).HasColumnName("token").HasColumnType("varchar(100)");

                e.HasKey(x => x.Id).HasName("users_pk");
                e.HasIndex(x => x.UserName).IsUnique().HasName("users_ui1");
            });
            modelBuilder.Entity<User>().ToTable("users_auth");
        }
    }
}