﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using AutoMapper;
using GlobalLoop.Authorization.Data.Entities;

namespace GlobalLoop.Authorization.Data.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserBaseDto, User>(MemberList.None)
                .ForMember(m => m.UserName, cfg => cfg.MapFrom(x => x.UserName))
                .ForMember(m => m.FirstName, cfg => cfg.MapFrom(x => x.IdentityName.FirstName))
                .ForMember(m => m.LastName, cfg => cfg.MapFrom(x => x.IdentityName.LastName))
                .ReverseMap()
                .ForMember(m => m.Email, cfg => cfg.MapFrom(x => x.UserName));

            CreateMap<User, UserDto>()
                .IncludeBase<User, UserBaseDto>()
                .ForMember(m => m.Id, cfg => cfg.MapFrom(x => x.Id));

            CreateMap<UserDto, User>(MemberList.None)
                .IncludeBase<UserBaseDto, User>()
                .ForMember(m => m.Id, cfg => cfg.MapFrom(x => x.Id));

            CreateMap<User, UserFullDto>()
                .IncludeBase<User, UserDto>()
                .ForMember(m => m.IsDisabled, cfg => cfg.MapFrom(x => x.IsDisabled));
        }
    }
}