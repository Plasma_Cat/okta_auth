﻿// © GlobalLoop. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.

using GlobalLoop.Authorization.Data.Entities;
using Microsoft.Extensions.Logging;

namespace GlobalLoop.Authorization.Data
{
    public static class EntitiesExtensions
    {
        public static void ValidateOktaIdIsNotNull(this User user, ILogger logger)
        {
            if (user.OktaId == null)
            {
                logger.LogCritical($"User {user.Id} has no OktaId.");
                throw new GlobalLoopAuthorizationException(20002, user.Id);
            }
        }
    }
}